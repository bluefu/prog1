package arrayrotate;

import java.util.Arrays;
import java.util.Scanner;

public class RotateArray 
{
	
	public static void printArr(int[][] arr)
	{
		for(int[] row:arr)
			System.out.println(Arrays.toString(row));
	}
	
	public static void main(String[] args)
	{
		int rows, cols;
		Scanner sc = new Scanner(System.in);
		System.out.println("Zeilen:");
		rows = sc.nextInt();
		System.out.println("Spalten:");
		cols = sc.nextInt();
		
		// Arrayerstellung
		int[][] arr = new int[rows][cols];
		System.err.println(arr);
		
		// Initialisierung
		for(int i = 0, cnt = 0; i<rows; i++)
			for(int j = 0; j<cols; j++,  cnt++)
				arr[i][j] = cnt;
		
		printArr(arr);
		
		// Rotieren 
		rows = arr[0].length;
		cols = arr.length;
		
		int[][] rotate = new int[rows][cols];
		
		// Zeilen von unten nach oben durchiterieren
		for(int ic = 0; ic<rows; ic++)
			for(int jc = 0, j = cols-1 ; jc<cols; jc++, j--)
				rotate[ic][jc] = arr[j][ic];
			
		arr = rotate;
		
		printArr(arr);
		
		sc.close();
	}
}
