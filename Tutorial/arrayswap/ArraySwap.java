package arrayswap;

import java.util.Arrays;

public class ArraySwap 
{
	public static void main(String[] args)
	{
		char[] c = {'a', 'b', 'c', 'd' };
		char[] d = {'x', 'y' };
		char[] swap = c;
		
		System.out.println(Arrays.toString(c));
		System.out.println(Arrays.toString(d))
		;
		System.out.println("Swap:");
		
		c = d;
		d = swap;
		
		System.out.println(Arrays.toString(c));
		System.out.println(Arrays.toString(d));
		
		System.out.println();
		
	}
	
}
