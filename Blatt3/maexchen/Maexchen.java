package maexchen;

import java.util.Scanner;

public class Maexchen 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		int wurf1, wurf2, erg;
		
			do
			{
				System.out.println("1.Wurf. Zahl eingeben(1-6)");
				wurf1 = sc.nextInt();
			}while(wurf1<1 || wurf1>6);
			
			do
			{
				System.out.println("2.Wurf. Zahl eingeben(1-6)");
				wurf2 = sc.nextInt();
			}while(wurf2<1 || wurf2>6);
	
			if((wurf1 == 1 && wurf2 == 2) || (wurf1 == 2 && wurf2 == 1))
				erg = 1000;
			else if(wurf1 == wurf2)
				erg = wurf1*100;
			else
			{
				if(wurf1>wurf2)
					erg = wurf1*10+wurf2;
				else
					erg = wurf2*10+wurf1;
			}
			System.out.println("Ergebnis: " + erg);
			System.out.println();
		
		sc.close();
	}

}
