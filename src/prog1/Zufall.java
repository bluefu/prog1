package prog1;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Zufall
{
	public static void main(String[] args){
		long seed = System.nanoTime();
		Integer[] li = {1,2,3,4,5,6,7,8};
		List<Integer> l = Arrays.asList(li);
		Collections.shuffle(l, new Random(seed));
		System.out.println(l);
	}
}
