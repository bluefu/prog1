package prog1;

import java.util.Scanner;

public class GGT 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		int a,b,r;
		a = sc.nextInt();
		b = sc.nextInt();
		r = a%b;
		
		while(r != 0)
		{
			a = b;
			b = r;
			r = a%b;
		}
		System.out.println("GGT: " +  b);
		sc.close();
		
	}

}
