package prog1;

public class Punkte 
{
	private int x;
	private int y;
	public Punkte(int x, int y){this.x = x; this.y = y;}
	public String toString()
	{
		return "("+x+","+y+")";
	}
	
	public static void vertauschePunkte(Punkte p1, Punkte p2)
	{
		System.out.println(p1 +" - "+p2);
		Punkte tmp = p1;
		p1 = p2;
		p2 = tmp;
		System.out.println(p1 +" - "+p2);
		
	}
	
	public static void main(String[] args)
	{
		Punkte x = new Punkte(5, 8);
		Punkte y = new Punkte(-1,1);
		
		System.out.println(x +" "+y);
		
		vertauschePunkte(x,y);
		
		System.out.println(x +" "+y);
	}
}
