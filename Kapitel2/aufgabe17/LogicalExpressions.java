package aufgabe17;

import java.util.Scanner;

public class LogicalExpressions 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		/* --- (a) --- */
		int a,b,c,d;
		boolean result;

		/* --- (b) --- */
		System.out.println("4 Zahlen mit Leerzeichen getrennt eingeben (a b c d):");
		a = sc.nextInt();
		b = sc.nextInt();
		c = sc.nextInt();
		d = sc.nextInt();
		
		/* --- (c) --- */
		System.out.println("A:" + a + " B:" + b + " C:" + c + " D:" + d);
		
		/* Aussage 1 */
		System.out.println("Aussage 1: Mindestens eine der Variablen a,b,c,d hat einen Wert > 1");
		
		result = (a>1 || b>1 || c>1 || d>1);		
		System.out.println("Ergebnis 1: " + result);
		
		/* Aussage 2 */
		System.out.println(	"Aussage 2: Mindestes eine, jedoch h�chstens 3"
							+ "der Variablen haben einen Wert > 1");
		
		result = 	(a>1 || b>1 || c>1 || d>1) 
					&&
					!(a>1 && b>1 && c>1 && d>1);
		System.out.println("Ergebnis 2: " + result);
			
		/* Aussage 3 */
		System.out.println("Aussage 3: Genau eine der Variablen hat einen Wert < 0");
		
		result =	((a<0) && !(b<0) && !(c<0) && !(d<0))
					||
					((b<0) && !(a<0) && !(c<0) && !(d<0))
					||
					((c<0) && !(a<0) && !(b<0) && !(d<0))
					||
					((d<0) && !(a<0) && !(b<0) && !(c<0));
		System.out.println("Ergebnis 3: " + result);
		
		/* Aussage 4 */
		System.out.println(	"Aussage 4: Alle Variablen, deren Werte > 0 sind, "
							+ "sind auch > 10");
		
		if(	(a>0) && !(a>10) 
			||
			(b>0) && !(b>10) 
			||
			(c>0) && !(c>10) 
			||
			(d>0) && !(d>10))
		{
			result = false;
		}
		else
		{
			result = true;
		}
		System.out.println("Ergebnis 4: " + result);			
		
		sc.close();
		
	}
}
