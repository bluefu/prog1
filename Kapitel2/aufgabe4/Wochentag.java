package aufgabe4;

import java.util.Scanner;

public class Wochentag 
{
	// minimales Fehlerhandling
	public static boolean korrektesDatum(int day, int month, int year)
	{
		if(schaltjahr(year))
		{
			if ((month == 2) && (day <= 29))
				return true;
			
			if((month == 2) && (day>29))
			{
				System.out.printf("Datum nicht m�glich (Info: %d ist ein Schaltjahr). Erneut eingeben!\n", year);
				return false;
			}
		}
		else
		{
			if ((month == 2) && (day <= 28))
				return true;
			if ((month == 2) && (day > 28))
			{
				System.out.printf("Datum nicht m�glich. Erneut eingeben!\n", year);
				return false;
			}
		}
		
		if ((month % 2 == 1) && (month <= 7))
			return true;
		else if ((month % 2 == 0) && (month > 7))
			return true;
		else if ((month != 2) && (day < 31))
			return true;
		else
		{
			System.out.printf("Datum nicht m�glich. Erneut eingeben!\n");
			return false;
		}
	}
	
	// Schaltjahrpr�fung
	public static boolean schaltjahr(int year)
	{
		if((year % 4) == 0 && (year % 100) != 0 )
			return true;
		if(year % 400 == 0)
			return true;
		return false;
	}

	public static void main(String[] args) 
	{
		// d = Tag, m = Monat, y = Jahr im Jahrzehnt, c = Jahrhundert, w = Wochentag, fullYear = Jahr
		int d,m,y,c,w,fullYear;
		Scanner sc = new Scanner(System.in);
		
		do
		{
			do
			{
				System.out.println("Tag eingeben (1-31):");
				d = sc.nextInt();
			} while(d<1 || d>31);
			
			do
			{
				System.out.println("Monat eingeben (1-12):");
				m = sc.nextInt();
			} while(m<1 || m>12);
			System.out.println("Jahr eingeben:");
			fullYear = sc.nextInt();
		} while(korrektesDatum(d,m,fullYear)==false);
		
		y = fullYear % 100;
		c = fullYear / 100;
		
		// falls der Monat <=2, soll der entsprechende Monat des Vorjahres gew�hlt werden
		if(m<=2)
		{
			m+=12;
			// Fehlerhandling, falls das Jahr 0 ist und du das Vorjahr 99 sein soll.
			if(y > 0)
				y--;
			else
			{
				y = 99;
				c--;
			}	
		}
		
		w = (d + (26*(m+1))/10 + (5*y)/4 + c/4 + 5*c-1)%7;
		
		if(m>12)
			m-=12;
		System.out.printf("Der %d.%d.%d ist ein ",d,m,fullYear);
		
		switch(w)
		{
		case 0: 	System.out.println("Sonntag."); 	break;
		case 1: 	System.out.println("Montag."); 		break;
		case 2: 	System.out.println("Dienstag."); 	break;
		case 3: 	System.out.println("Mittwoch."); 	break;
		case 4: 	System.out.println("Donnerstag."); 	break;
		case 5: 	System.out.println("Freitag."); 	break;
		case 6: 	System.out.println("Samstag."); 	break;
		default: 	System.err.println("Fehler.");
		}
		
		sc.close();
	}

}
